#include "hello.hpp"

using namespace godot;

void Hello::_register_methods()
{
    register_method("_ready", &Hello::_ready);
    register_method("_process", &Hello::_process);
}

void Hello::_init()
{
}

void Hello::_ready()
{
    Godot::print("Hello World!");
}

void Hello::_process(float delta)
{
}

Hello::Hello()
{
}

Hello::~Hello()
{
}
